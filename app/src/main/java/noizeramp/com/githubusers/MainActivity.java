package noizeramp.com.githubusers;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.eclipse.egit.github.core.User;
import org.eclipse.egit.github.core.service.UserService;

import java.io.IOException;
import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements SearchView.OnQueryTextListener {

    private ArrayList<User> users = new ArrayList<>();
    private ArrayAdapter<User> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SearchView searchView = (SearchView) findViewById(R.id.search);
        searchView.setOnQueryTextListener(this);

        ListView listView = (ListView) findViewById(R.id.list);
        adapter = new ArrayAdapter<User>(MainActivity.this, R.layout.list_item_user, users) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                User user = users.get(position);
                View view = convertView;

                if (view == null) view = getLayoutInflater().inflate(R.layout.list_item_user, null);

                TextView nameView = (TextView) view.findViewById(R.id.name);
                TextView emailView = (TextView) view.findViewById(R.id.email);
                TextView publicReposView = (TextView) view.findViewById(R.id.public_repos);

                nameView.setText(user.getName());
                emailView.setText(user.getEmail());
                publicReposView.setText(Integer.toString(user.getPublicRepos()));

                String imageURL = user.getAvatarUrl();
                //imageURL = imageURL.replace("https", "http");

                ImageView avatarView = (ImageView) view.findViewById(R.id.avatar);
                Picasso.with(MainActivity.this).load(imageURL).fit().centerCrop().into(avatarView);

                return view;
            }
        };
        listView.setAdapter(adapter);
    }

    private void onNewUser(User user) {
        users.add(0, user);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        new SearchForUser().execute(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return true;
    }

    public class SearchForUser extends AsyncTask<String, Void, User> {
        @Override
        protected User doInBackground(String... params) {
            UserService userService = new UserService();
            try {
                return userService.getUser(params[0]);
            } catch (IOException e) {
                Log.e(MainActivity.class.getName(), "Failed to load user " + params[0] + " with message: " + e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(User user) {
            if (user != null) {
                onNewUser(user);
            } else {
                Toast.makeText(MainActivity.this, "User not found", Toast.LENGTH_SHORT);
            }
        }
    }
}
